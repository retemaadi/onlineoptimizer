package edu.leiden.aqosa.servlet;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.simple.parser.ParseException;

import com.google.api.client.util.Base64;

import edu.leiden.aqosa.google.auth.GoogleAuthHelper;

@SuppressWarnings("serial")
public class OAuth2CallbackServlet extends HttpServlet {

	@Override
	public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		boolean local = (request.getLocalPort() == 8080);
		final GoogleAuthHelper helper = new GoogleAuthHelper(local);
		
		response.setContentType("text/html");
		PrintWriter writer = response.getWriter();
		writer.println("<html>");
		writer.println("<head>");
		
		Cookie cookie = null;
		try {
			String userinfo = helper.getUserInfoJson(request.getParameter("code"));
			String cookvalue = new String( Base64.encodeBase64(userinfo.getBytes()) );
			cookie = new Cookie("userinfo", cookvalue);
		} catch (ParseException e) {
			e.printStackTrace();
		}
		cookie.setMaxAge(60*60*24);		//1 day
		response.addCookie(cookie);
		
		writer.println("<meta HTTP-EQUIV='REFRESH' content='0; url=/'>");
		writer.println("</head>");		
		
		writer.println("<body>");
		writer.println("</body>");
		writer.println("</html>");
	}
	
}
