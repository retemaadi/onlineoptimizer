package edu.leiden.aqosa.servlet;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.zip.ZipOutputStream;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.tools.zip.ZipEntry;

import com.google.api.client.util.Base64;

import edu.leiden.aqosa.google.auth.UserInfo;

@SuppressWarnings("serial")
public class DownloadServlet extends HttpServlet {

	@Override
	public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		Cookie cookies[] = request.getCookies();
		Cookie cook = null;
		if (cookies != null) {
			for (int i = 0; i < cookies.length; i++) {
				if (cookies[i].getName().equals("userinfo")) {
					cook = cookies[i];
					break;
				}
			}
		}

		if (cook == null || cook.getValue() == null) {
			response.sendRedirect("/login");
			return;
		}
		
		UserInfo userInfo = new UserInfo(new String( Base64.decodeBase64(cook.getValue()) )); 
		String userId = userInfo.getUserEmail();
		
		ServletContext servletContext = this.getServletConfig().getServletContext();
		File userFolder = new File((File) servletContext.getAttribute("javax.servlet.context.tempdir"), userId);
		File logFolder = new File(userFolder, "logger");
		
		try {
			File[] listOfFiles = logFolder.listFiles();
			
			String fname = userId + ".zip";
			response.setContentType("application/x-download");
			response.setHeader("Content-Disposition", "attachment; filename=" + fname);
		
			byte lineSeparator = 10;
			String sCurrentLine;
			
			BufferedReader reader;
			ZipOutputStream zos = new ZipOutputStream( response.getOutputStream() );
			
			for (File file : listOfFiles) {
				String filename = file.getName();
					
				reader = new BufferedReader( new FileReader(file) );
				
				zos.putNextEntry(new ZipEntry( filename ));
				while ((sCurrentLine = reader.readLine()) != null) {
					zos.write(sCurrentLine.getBytes());
					zos.write(lineSeparator);
				}
				
				zos.flush();
				zos.closeEntry();
				reader.close();
			}
			
			zos.close();
		} catch (IOException e) {
			e.printStackTrace();
			throw new ServletException("IOException", e);
		}
	}
	
}
