package edu.leiden.aqosa.servlet;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.io.FileUtils;

import com.google.api.client.util.Base64;

import edu.leiden.aqosa.google.auth.UserInfo;
import edu.leiden.aqosa.launcher.MatingApproach;
import edu.leiden.aqosa.launcher.MultipleRun;
import edu.leiden.aqosa.servlet.listener.OptIterationListener;
import edu.leiden.aqosa.servlet.printer.CloudPrinter;

@SuppressWarnings("serial")
public class OptimizeServlet extends HttpServlet {

	@Override
	public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		Cookie cookies[] = request.getCookies();
		Cookie cook = null;
		if (cookies != null) {
			for (int i = 0; i < cookies.length; i++) {
				if (cookies[i].getName().equals("userinfo")) {
					cook = cookies[i];
					break;
				}
			}
		}

		if (cook == null || cook.getValue() == null) {
			response.sendRedirect("/login");
			return;
		}
		
		UserInfo userInfo = new UserInfo(new String( Base64.decodeBase64(cook.getValue()) )); 
		String userId = userInfo.getUserEmail();
		
		ServletContext servletContext = this.getServletConfig().getServletContext();
		File userFolder = new File((File) servletContext.getAttribute("javax.servlet.context.tempdir"), userId);
		File logFolder = new File(userFolder, "logger");
		FileUtils.deleteDirectory(logFolder);
		logFolder.mkdir();
		
		response.setContentType("text/event-stream");
		response.setHeader("Cache-Control", "no-cache");
		response.setHeader("Connection", "keep-alive");		
		CloudPrinter printer = new CloudPrinter( response.getOutputStream() );
		try {
			int exp = Integer.parseInt( request.getParameter("exp") );
			int generations = Integer.parseInt( request.getParameter("generations") );
			int alpha = Integer.parseInt( request.getParameter("alpha") );
			int mu = Integer.parseInt( request.getParameter("mu") );
			int lambda = Integer.parseInt( request.getParameter("lambda") );
			int archiveSize = Integer.parseInt( request.getParameter("archiveSize") );
			
			FileInputStream stream = new FileInputStream(new File(userFolder, "model.aqosa"));
			MultipleRun run = new MultipleRun(exp, stream, logFolder.getAbsolutePath(), "archive", "optimum");
			if (run != null) {
				run.setEA(generations, alpha, mu, lambda);
				run.setArchiveSize(archiveSize);
				run.setMatingAppraoch(MatingApproach.CROSSOVER_MUTATE);
				run.setEvaluationLogger(printer);
				
				run.addOptimizerIterationListener(OptIterationListener.class);
				
				try {
					run.call();
				} catch (Exception e) {
					e.printStackTrace(printer);
					throw new ServletException("Cannot run the optimization process.", e);
				}				
			}
			
			printer.sendFinish();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			printer.close();
		}

	}

}
