package edu.leiden.aqosa.servlet.printer;

import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.List;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.opt4j.core.Individual;
import org.opt4j.core.Value;
import org.opt4j.core.optimizer.Archive;

public class CloudPrinter extends PrintStream {

	protected final OutputStream outputStream;
	protected List<String> logs = new ArrayList<String>();
	
	public CloudPrinter(OutputStream outputStream) {
		super( outputStream );
		this.outputStream = outputStream;
	}

	@Override
	public void println(String log) {
		String msg = log + "\n";
		logs.add(msg);		
	}
	
	@SuppressWarnings("unchecked")
	public void flushPrint(Archive archive, int iteration) {
		JSONObject json = new JSONObject();
		JSONArray log = new JSONArray();
		for (String l : logs) {
			log.add(l);
		}
		json.put("log", log);
		logs.clear();
		
		JSONArray arr = new JSONArray();
		for (Individual ind : archive) {
			JSONArray row = new JSONArray();
			for (Value<?> v : ind.getObjectives().getValues()) {
				row.add(v.getDouble());
			}
			arr.add(row);
		}
		json.put("archive", arr);
		
		super.println("data: " + json + "\n\n");
		try {
			outputStream.flush();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	@SuppressWarnings("unchecked")
	public void clearLogs() {
		JSONObject json = new JSONObject();
		JSONArray log = new JSONArray();
		for (String l : logs) {
			log.add(l);
		}
		json.put("log", log);
		logs.clear();
		
		super.println("data: " + json + "\n\n");
		try {
			outputStream.flush();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public void sendFinish() {
		clearLogs();
		super.println("data:__END__\n\n");
		try {
			outputStream.flush();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
