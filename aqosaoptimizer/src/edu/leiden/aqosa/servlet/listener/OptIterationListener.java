package edu.leiden.aqosa.servlet.listener;

import org.opt4j.core.optimizer.Archive;
import org.opt4j.core.optimizer.OptimizerIterationListener;

import com.google.inject.Inject;

import edu.leiden.aqosa.logger.EvaluationLogger;
import edu.leiden.aqosa.servlet.printer.CloudPrinter;

public class OptIterationListener implements OptimizerIterationListener {

	protected Archive archive;
	protected CloudPrinter printer;
	
	@Inject
	public OptIterationListener(Archive archive, EvaluationLogger logger) {
		this.archive = archive;
		this.printer = (CloudPrinter) logger.getStream();
	}
	
	@Override
	public void iterationComplete(int iteration) {
		printer.flushPrint(archive, iteration);
	}

}
