package edu.leiden.aqosa.servlet;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Collections;
import java.util.List;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
import org.json.simple.JSONArray;

import com.google.api.client.util.Base64;

import edu.leiden.aqosa.google.auth.UserInfo;
import edu.leiden.aqosa.launcher.ModelReader;
import edu.leiden.aqosa.model.IR.AQOSAModel;
import edu.leiden.aqosa.model.IR.Evaluations;

@SuppressWarnings("serial")
public class UploadServlet extends HttpServlet {

	@SuppressWarnings("unchecked")
	@Override
	public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		Cookie cookies[] = request.getCookies();
		Cookie cook = null;
		if (cookies != null) {
			for (int i = 0; i < cookies.length; i++) {
				if (cookies[i].getName().equals("userinfo")) {
					cook = cookies[i];
					break;
				}
			}
		}

		if (cook == null || cook.getValue() == null) {
			response.sendRedirect("/login");
			return;
		}
		
		UserInfo userInfo = new UserInfo(new String( Base64.decodeBase64(cook.getValue()) )); 
		String userId = userInfo.getUserEmail();
		
		ServletContext servletContext = this.getServletConfig().getServletContext();
		File userFolder = new File((File) servletContext.getAttribute("javax.servlet.context.tempdir"), userId);
		if (!userFolder.exists())
			userFolder.mkdir();
		File modelFile = new File(userFolder, "model.aqosa");
		if (modelFile.exists())
			modelFile.delete();
		
		ServletFileUpload upload = new ServletFileUpload(new DiskFileItemFactory());
		try {
			List<FileItem> items = upload.parseRequest(request);
			for (FileItem item : items) {
				if (!item.isFormField()) {
					modelFile.createNewFile();
					item.write(modelFile);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		AQOSAModel model = ModelReader.readModel(new FileInputStream(modelFile));
		List<Evaluations> evals = model.getObjectives().getSettings().getEvaluations();
		
		JSONArray objs = new JSONArray();
		for (Evaluations ev : evals) {
			objs.add(ev.getName());
		}
		
		Collections.sort(objs);
		response.getWriter().println(objs);
	}

}
