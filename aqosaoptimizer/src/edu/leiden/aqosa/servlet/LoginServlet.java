package edu.leiden.aqosa.servlet;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import edu.leiden.aqosa.google.auth.GoogleAuthHelper;

@SuppressWarnings("serial")
public class LoginServlet extends HttpServlet {

	@Override
	public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		boolean local = (request.getLocalPort() == 8080);
		final GoogleAuthHelper helper = new GoogleAuthHelper(local);
		
		response.setContentType("text/html");
		PrintWriter writer = response.getWriter();
		writer.println("<html>");
		writer.println("<head>");
		writer.println("</head>");
		writer.println("<body>");
		
		writer.println("<div align='center'>");
		writer.println("<a href='" + helper.buildLoginUrl()+ "'><img alt='Signin with Google' src='https://developers.google.com/+/images/branding/sign-in-buttons/White-signin_Long_base_44dp.png'></a>");
		writer.println("</div>");

		writer.println("</body>");
		writer.println("</html>");
	}
	
}
