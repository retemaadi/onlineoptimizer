package edu.leiden.aqosa.google.auth;

import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

public class UserInfo {
	
	protected final String userInfo;
	protected final JSONObject json;
	
	public UserInfo(String info) {
		this.userInfo = info;
		
		JSONParser parser = new JSONParser();
		Object data = new Object();
		try {
			data = parser.parse( info );
		} catch (ParseException e) {
			e.printStackTrace();
		}
		this.json = (JSONObject) data;
	}
	
	public String getString() {
		return userInfo;
	}
	
	public JSONObject getJson() {
		return json;
	}
	
	public String getUserEmail() {
		return (String) json.get("email");
	}
	
	public String getFullName() {
		return (String) json.get("name");
	}
	
	@Override
	public String toString() {
		return getString();
	}
}
