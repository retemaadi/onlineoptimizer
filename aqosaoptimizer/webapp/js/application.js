var eventSource;
var uploadUrl = '/upload';
var optzUrl = '/optimize';
var downUrl = '/download';

var startBtn = $('#startbtn');
var downloadBtn = $('#downloadbtn');
var obj1 = $('#obj1')[0];
var obj2 = $('#obj2')[0];

var arr = new Array(['Objective X', 'Objective Y'], [1.0, 1.0]);
var options = {
	title: 'Pareto front',
	hAxis: {title: 'Objective X'},
	vAxis: {title: 'Objective Y'},
	legend: 'none'
};

var archive;

$(document).ready(function() {
	window.location.assign("#optimize");
	
	disableButton(startBtn);
	disableButton(downloadBtn);
	
    // Custom selects
    $("select").dropkick();
    
	google.load("visualization", "1", {
		packages: ["corechart"],
		callback: 'drawChart'
	});
	
    // Todo list
    /*
    $(".todo li").click(function() {
        $(this).toggleClass("todo-done");
    });
    */
    
    // Init tooltips
    /*
    $("[data-toggle=tooltip]").tooltip("show");
    */
    
    // Init tags input
    /*
    $("#tagsinput").tagsInput();
    */
    
    // Init jQuery UI slider
    /*
    $("#slider").slider({
        min: 1,
        max: 5,
        value: 2,
        orientation: "horizontal",
        range: "min",
    });
    */
    
    // JS input/textarea placeholder
    /*
    $("input, textarea").placeholder();
    */

    // Make pagination demo work
    /*
    $(".pagination a").click(function() {
        if (!$(this).parent().hasClass("previous") && !$(this).parent().hasClass("next")) {
            $(this).parent().siblings("li").removeClass("active");
            $(this).parent().addClass("active");
        }
    });
    */
    
    /*
    $(".btn-group a").click(function() {
        $(this).siblings().removeClass("active");
        $(this).addClass("active");
    });
    */
    
    // Disable link click not scroll top
    $("a[href='#']").click(function() {
        return false
    });

});

var disableButton = function(btn) {
	btn.removeClass('btn-success').addClass('disabled');
}

var enableButton = function(btn) {
	btn.removeClass('disabled').addClass('btn-success');
}

var fileInput = $(':file').wrap($('<div/>').css({height:0, width:0, 'overflow':'hidden'}));
$('#file').click(function(){
	fileInput.click();
}).show();

fileInput.change(function(){
	var name = $(this)[0].files[0].name;
	var trueFile = name.indexOf('.aqosa');
	if (trueFile != -1) {
		$('#error').css({'display':'none'});
		
		$('#file').html("<h3>" + name + "</h3>");
		
		var uploadForm = document.getElementById('uploadForm');
		var	request = new XMLHttpRequest();
		request.open('POST', uploadUrl);
		var fd = new FormData(uploadForm);
		request.send(fd);
		
		request.onreadystatechange = function(){
			if (request.readyState==4 && request.status==200) {
				var objs = JSON.parse(request.responseText);
				updateObjectives(objs);
				
				enableButton(startBtn);
			}
		}
	} else {
		$('#file').html("<h3>Upload Model File</h3>");
		disableButton(startBtn);
		
		$('#error').css({'display':'block'});
		$('#error').text('Wrong file!!! please select an AQOSA file.');
	}
});

var updateObjectives = function(objs) {
	var dk_ob1 = document.getElementById('dk_container_obj1');
	dk_ob1.parentNode.removeChild(dk_ob1);
	var dk_ob2 = document.getElementById('dk_container_obj2');
	dk_ob2.parentNode.removeChild(dk_ob2);
	
	obj1.options.length = 0;
	obj2.options.length = 0;
	
	for(var i=0; i<objs.length; i++) {
		var op1 = document.createElement("option");
		var op2 = document.createElement("option");
		op1.value = i;
		op2.value = i;
		
		op1.innerHTML = objs[i];
		op2.innerHTML = objs[i];
		
		obj1.appendChild(op1);
		obj2.appendChild(op2);
	}
	
	obj1.options[0].selected = 'selected';
	obj2.options[objs.length-1].selected = 'selected';
	updateDropdown();
	
	$("select").dropkick();
}

$('#exp').change(function() {
	$('#expValue').html($(this)[0].value);
});

$('#generations').change(function() {
	$('#generationsValue').html($(this)[0].value);
});

$('#alpha').change(function() {
	$('#alphaValue').html($(this)[0].value);
});

$('#mu').change(function() {
	$('#muValue').html($(this)[0].value);
});

$('#lambda').change(function() {
	$('#lambdaValue').html($(this)[0].value);
});

$('#archiveSize').change(function() {
	$('#archiveSizeValue').html($(this)[0].value);
});

var updateDropdown = function() {
	var xTitle = obj1.options[obj1.value].text;
	var yTitle = obj2.options[obj2.value].text;
	options.title = xTitle + ' vs. ' + yTitle + ' Pareto front';
	options.hAxis.title = xTitle;
	options.vAxis.title = yTitle;
	updateChart();
}

var updateChart = function() {
	if (archive != null) {
		arr = new Array(['Objective X', 'Objective Y']);
		
		for(var j=0; j<archive.length; j++) {
			var row = archive[j];
			arr[j+1] = new Array(row[obj1.value], row[obj2.value]);
		}
	}
	
	drawChart();
}

var drawChart = function() {
	var data = google.visualization.arrayToDataTable(arr);
	var chart = new google.visualization.ScatterChart(document.getElementById('chart_div'));
	chart.draw(data, options);
}

var handleEventMessage = function(event) {
	var msg = event.data;
	if (msg == '__END__') {
		eventSource.close();
	    
		disableButton(startBtn);
		enableButton(downloadBtn);
	} else {
		var data = JSON.parse(msg);
		var logs = data.log;
		if (data.archive != null)
			archive = data.archive;
		
		var oldlog = $('#output').text();
		var count = oldlog.match(/\n/g);
		if (count != null && count.length > 299) {
			var n = count.length - 299;
		    while (n > 1) {
		        var index = oldlog.lastIndexOf('\n');
		        oldlog = oldlog.substring(index);
		        n--;
		    }
			$('#output').text(oldlog);
		}
		
		for(var j=0; j<logs.length; j++) {
			var newlog = $('#output').text();
			$('#output').text(logs[j] + newlog);
		}
		
		updateChart();
	}
}

startBtn.click(function() {
	window.location.assign("#pareto");
	disableButton(startBtn);
	
	var eventUrl = optzUrl + '?' +
		'exp=' + $('#exp')[0].value + '&' +
		'generations=' + $('#generations')[0].value + '&' +
		'alpha=' + $('#alpha')[0].value + '&' +
		'mu=' + $('#mu')[0].value + '&' +
		'lambda=' + $('#lambda')[0].value + '&' +
		'archiveSize=' + $('#archiveSize')[0].value;
	
	eventSource = new EventSource(eventUrl);
	eventSource.addEventListener('message', handleEventMessage, false);
	eventSource.addEventListener('open', function(event) {
		// Connection was opened.
	}, false);
	eventSource.addEventListener('error', function(event) {
		if (event.readyState == EventSource.CLOSED) {
			// Connection was closed.
			console.log('Connection closed');
	  }
	}, false);
});

downloadBtn.click(function() {
	var downloadForm = $('#downloadForm')[0];
	downloadForm.action = downUrl;
	downloadForm.submit();
});
