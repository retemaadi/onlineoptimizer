<%@page import="com.google.api.client.util.Base64,edu.leiden.aqosa.google.auth.UserInfo"%><%
Cookie cookies[] = request.getCookies();
Cookie cook = null;
if (cookies != null) {
	for (int i = 0; i < cookies.length; i++) {
		if (cookies[i].getName().equals("userinfo")) {
			cook = cookies[i];
			break;
		}
	}
}
if (cook == null || cook.getValue() == null) {
	response.sendRedirect("/login");
} else {
	UserInfo userInfo = new UserInfo(new String( Base64.decodeBase64(cook.getValue()) )); 
	String user = userInfo.getFullName();
%><!DOCTYPE html>

<html>
<head>
	<title>AQOSA - Online Optimizer</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
	
    <link rel="stylesheet" type="text/css" href="css/bootstrap.css" />
	<!-- Credit to: http://designmodo.com/flat-free/ -->
    <link rel="stylesheet" type="text/css" href="css/flat-ui.css" />
	<!-- Credit to: http://www.sitepoint.com/css3-tabs-using-target-selector/ -->
	<link rel="stylesheet" type="text/css" href="css/main.css" />
</head>

<body>

<div id="page">
	<div id="welcome">
		<h4>Welcome <%= user %>,</h4>
	</div>
	<article class="tabs">

		<section id="design">
			<h2><a href="#design">Design</a></h2>
			<div>
				<div class="innerDiv">
					<p style="font-size: 1.3em;">Design your software! This section will coming soon ...</p>
					<p>For the time being, you can download sample files from these urls: <br /><br />
						
						<a href="/model/CRN.aqosa">Car-Radio-Navigation sample</a> <br />
						<a href="/model/CruiseControl.aqosa">Car-Cruise-Control sample</a> <br />
					</p>
				</div>
				<p class="tabnav"><a href="#optimize" class="btn btn-large btn-block btn-warning">Jump to Optimizer Page &#8658;</a></p>
			</div>
		</section>

		<section id="optimize">
			<h2><a href="#optimize">Optimize</a></h2>
			<div>
				<div id="contact-form" class="innerDiv">
					<p id="error" style="color: red;"> </p>
					<table>
						<tr>
							<td class="onecol" colspan="2" align="center">
								<form id="uploadForm" method="POST" enctype="multipart/form-data">
								<div id="file"><h3>Upload Model File</h3></div>
								<input type="file" name="model" id="modelFile" />
								</form>
							</td>
						</tr>
						<tr>
							<td class="col1"><h4>No. Experiments: <span id="expValue">2</span></h4></td>
							<td class="col2"><input type="range" name="exp" id="exp" min="1" max=10 value="2" step="1" /></td>
						</tr>
						<tr>
							<td class="col1"><h4>Generations: <span id="generationsValue">50</span></h4></td>
							<td class="col2"><input type="range" name="generations" id="generations" min="50" max="10000" value="50" step="50" /></td>
						</tr>
						<tr>
							<td class="col1"><h4>Alpha: <span id="alphaValue">25</span></h4></td>
							<td class="col2"><input type="range" name="alpha" id="alpha" min="10" max="100" value="25" step="5" /></td>
						</tr>
						<tr>
							<td class="col1"><h4>Mu: <span id="muValue">10</span></h4></td>
							<td class="col2"><input type="range" name="mu" id="mu" min="5" max="50" value="10" step="5" /></td>
						</tr>
						<tr>
							<td class="col1"><h4>Lambda: <span id="lambdaValue">10</span></h4></td>
							<td class="col2"><input type="range" name="lambda" id="lambda" min="5" max="50" value="10" step="5" /></td>
						</tr>
						<tr>
							<td class="col1"><h4>Archive Size: <span id="archiveSizeValue">20</span></h4></td>
							<td class="col2"><input type="range" name="archiveSize" id="archiveSize" min="10" max="100" value="20" step="5" /></td>
						</tr>
					</table>					
					<div class="btndiv">
						<div class="btndivdiv">
							<button type="button" id="startbtn" class="btn btn-large btn-block btn-success">Start Optimization</button>
						</div>
					</div>
				</div>
				<!-- <p class="tabnav"><a href="#results" class="btn btn-large btn-block btn-warning">Go to Results Page &#10148;</a></p>  -->
			</div>
		</section>

		<section id="pareto">
			<h2><a href="#pareto">Pareto</a></h2>
			<div>
				<div id="chart" class="innerDiv">
					<div id="obj1div"><h4>X axis <select id="obj1" class="span3" tabindex="1" name="xlist"><option>Objective X</option></select></h4></div>
					<div id="obj2div"><h4>Y axis <select id="obj2" class="span3" tabindex="2" name="ylist"><option>Objective Y</option></select></h4></div>
					<div id="chart_div"></div>
				</div>	
				<p class="tabnav"><a href="#log" class="btn btn-large btn-block btn-info">See Log results &#8658;</a></p>
			</div>
		</section>

		<section id="log">
			<h2><a href="#log">Log</a></h2>
			<div>
				<div id="logger" class="innerDiv">
					<pre><code id="output"></code></pre>
					<form id="downloadForm" method="POST">
						<div class="btndiv">
							<div class="btndivdiv">
								<button type="button" id="downloadbtn" class="btn btn-large btn-block btn-success">Download Results!</button>
							</div>
						</div>
					</form>
				</div>
				<p class="tabnav"><a href="#pareto" class="btn btn-large btn-block btn-info">&#8656; See Pareto graphs </a></p>
			</div>
		</section>

		<section id="about">
			<h2><a href="#about">About</a></h2>
			<div>
				<div class="innerDiv">
					<p>About the AQOSA ...</p>
				</div>
				<p class="tabnav"><a href="#optimize" class="btn btn-large btn-block btn-warning">&#8656; Back to Optimize Page</a></p>
			</div>
		</section>

	</article>
</div>

<script type="text/javascript" src="https://www.google.com/jsapi"></script>
<script type="text/javascript" src="js/jquery-1.8.2.min.js"></script>
<script type="text/javascript" src="js/jquery-ui-1.10.0.custom.min.js"></script>
<script type="text/javascript" src="js/jquery.dropkick-1.0.0.js"></script><% /*
<script type="text/javascript" src="js/custom_checkbox_and_radio.js"></script>
<script type="text/javascript" src="js/custom_radio.js"></script>
<script type="text/javascript" src="js/jquery.tagsinput.js"></script>
<script type="text/javascript" src="js/bootstrap-tooltip.js"></script>
<script type="text/javascript" src="js/jquery.placeholder.js"></script> */ %>
<script type="text/javascript" src="js/application.js"></script>
</body>
</html><% } %>