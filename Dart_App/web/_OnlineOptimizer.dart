import 'dart:html';
import 'dart:math';
import 'js/js.dart' as js;

String uploadUrl = '/upload';
String optzUrl = '/optimize';
String downUrl = '/download';

EventSource eventSource;

ButtonElement startBtn = query('#startbtn');
ButtonElement downloadBtn = query('#downloadbtn');

InputElement modelFile = query('#modelFile');
InputElement generations = query('#generations');
InputElement alpha = query('#alpha');
InputElement mu = query('#mu');
InputElement lambda = query('#lambda');
InputElement archiveSize = query('#archiveSize');

int counter = 0;

void main() {
  modelFile.onChange.listen(handleFileChange);
  generations.onChange.listen(handleGenChange);
  alpha.onChange.listen(handleAlphaChange);
  mu.onChange.listen(handleMuChange);
  lambda.onChange.listen(handleLambdaChange);
  archiveSize.onChange.listen(handleArchiveChange);
  
  startBtn.disabled = true;
  startBtn.onClick.listen(handleStart);
  
  downloadBtn.disabled = true;
  downloadBtn.onClick.listen(handleDownload);
}

void outputMsg(String msg) {
  var output = query('#output');
  output.text = "${msg}\n${output.text}";
}

void errorMsg(String msg) {
  var error = query('#error');
  error.style.display = "block";
  error.text = "${msg}";
}

void handleFileChange(event) {
  String name = modelFile.files.first.name;
  bool trueFile = name.endsWith('.aqosa');
  if (trueFile) {
    errorMsg(' ');
    
    FormElement uploadForm = query('#uploadForm');
    HttpRequest request = new HttpRequest();
    request.open('POST', uploadUrl);
    FormData fd = new FormData(uploadForm);
    request.send(fd);
    
    startBtn.disabled = false;
  } else {
    errorMsg('Wrong file, please select an AQOSA file');
  }
}

void handleGenChange(event) {
  SpanElement value = query('#generationsValue');
  value.text = generations.value;
}

void handleAlphaChange(event) {
  SpanElement value = query('#alphaValue');
  value.text = alpha.value;
}

void handleMuChange(event) {
  SpanElement value = query('#muValue');
  value.text = mu.value;
}

void handleLambdaChange(event) {
  SpanElement value = query('#lambdaValue');
  value.text = lambda.value;
}

void handleArchiveChange(event) {
  SpanElement value = query('#archiveSizeValue');
  value.text = archiveSize.value;
}

void switchPage() {
  window.location.assign("#pareto");
}

void handleStart(event) {
  switchPage();
  startBtn.disabled = true;
  
  String eventUrl = optzUrl + '?' +
      'generations=' + generations.value + '&'
      'alpha=' + alpha.value + '&'
      'mu=' + mu.value + '&'
      'lambda=' + lambda.value + '&'
      'archiveSize=' + archiveSize.value;  
  eventSource = new EventSource(eventUrl);
  eventSource.onMessage.listen(handleEventMessage);
}

void handleEventMessage(MessageEvent me) {
  String msg = me.data;
  if (msg == '__END__') {    
    eventSource.close();
    
    startBtn.disabled = true;    
    downloadBtn.disabled = false;    
  } else {
    counter++;
    outputMsg(msg);
    
    if (counter == int.parse(lambda.value)) {
      counter = 0;
      
      js.context.google.load('visualization', '1', js.map(
          {
            'packages': ['corechart'],
            'callback': new js.Callback.once(drawVisualization)
          }));
    }
  }
}

void handleDownload(event) {
  FormElement downloadForm = query('#downloadForm');
  downloadForm.action = downUrl;
  downloadForm.submit();
}

void drawVisualization() {
  var gviz = js.context.google.visualization;

  //function drawChart() {
  //  var data = google.visualization.arrayToDataTable([
  //    [ 0.8,  0.12],
  //    [ 0.4,  0.55],
  //    [ 0.11, 0.14],
  //    [ 0.4,  0.5],
  //    [ 0.3,  0.35],
  //    [ 0.65, 0.7]
  //  ]);

  // Create and populate the data table.
  //var listData = [
  //   [ 0.8,  0.12],
  //   [ 0.4,  0.55],
  //   [ 0.11, 0.14],
  //   [ 0.4,  0.5],
  //   [ 0.3,  0.35],
  //   [ 0.65, 0.7]
  //];
  
  List listData = [['X','Y']];
  Random rng = new Random();
  for (var i = 0; i < int.parse(archiveSize.value); i++) {
    List p = [];
    p.add(rng.nextDouble());
    p.add(rng.nextDouble());
    
    listData.add(p);
  }  

  var arrayData = js.array(listData);
  var tableData = gviz.arrayToDataTable(arrayData);

  //  var options = {
  //    title: 'Cost vs. Cpu Utilization Pareto front',
  //    hAxis: {title: 'Cost', minValue: 0, maxValue: 1},
  //    vAxis: {title: 'Cpu Utilization', minValue: 0, maxValue: 1},
  //    legend: 'none'
  //  };
  var options = js.map({
    'title': 'Cost vs. Cpu Utilization Pareto front',
    'hAxis': {'title': 'Cost', 'minValue': 0, 'maxValue': 1},
    'vAxis': {'title': 'Cpu Utilization', 'minValue': 0, 'maxValue': 1},
    'legend': 'none'
  });
  
  // Create and draw the visualization.
  // var chart = new google.visualization.ScatterChart(document.getElementById('chart_div'));
  var chart = new js.Proxy(gviz.ScatterChart, query('#chart_div'));
  chart.draw(tableData, options);
}
